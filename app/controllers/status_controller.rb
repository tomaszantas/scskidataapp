class StatusController < ApplicationController
  def index
  end
  
  def get_status
    respond_to do |format|
      format.json do
        render json: {
          message: "Ski data status OK"
        }.to_json
      end
    end
  end
  
end
